@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Set Foo</div>
                    <div class="card-body">
                        @if ($errors->first('warning'))
                            <div class="alert alert-danger" role="alert">
                                Your Foo: {{ $errors->first('warning') }}
                            </div>
                        @endif
                        <form method="POST" action="{{ route('setFoo') }}">
                            @csrf
                            
                            <div class="form-group row">
                                <label for="foo" class="col-md-4 col-form-label text-md-right">Value for Foo</label>
                                
                                <div class="col-md-6">
                                    <input id="foo" type="foo" class="form-control @error('foo') is-invalid @enderror" name="foo" value="{{ old('foo') }}" autocomplete="foo" autofocus>
                                    
                                    @error('foo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">Set Foo</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
