<?php

namespace App\Console\Commands;

use App\Mail\SendMail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;

class SendPromo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:promo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'SendP Promo emails';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emails = Cache::get("emails");

        if (!is_array($emails) && count($emails) > 1) {
            return;
        }

        $list = array_shift($emails) ?: [];
        Cache::put("emails", $emails, now()->addDay());

        if (!is_array($list) && count($list) > 1) {
            return;
        }

        foreach ($list as $item) {
            //echo $item . "\n";
            Mail::to($item)->queue(new SendMail());
        }
    }
}