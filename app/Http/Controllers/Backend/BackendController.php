<?php
/**
 * Project      : phpExam
 * File Name    : Dashboard.php
 * User         : Abu Bakar Siddique
 * Email        : absiddique.live@gmail.com
 * Date[Y/M/D]  : 2019/05/24 10:37 AM
 */

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

class BackendController extends Controller
{
    public function __construct()
    {
        //$this->middleware('hasFoo')->except("setFoo");
    }
}