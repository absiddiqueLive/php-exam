<?php
/**
 * Project      : phpExam
 * File Name    : Dashboard.php
 * User         : Abu Bakar Siddique
 * Email        : absiddique.live@gmail.com
 * Date[Y/M/D]  : 2019/05/24 10:37 AM
 */

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SetFooController extends BackendController
{
    public function setFoo()
    {
        return view("backend.setFoo");
    }

    public function storeSetFoo(Request $request)
    {
        $this->validate($request, $this->getValidationRules(), $this->getValidationMessage());

        Session::put('foo', $request->input('foo'));

        return redirect()->route('dashboard');
    }

    public function getValidationRules()
    {
        return [
            'foo' => ['required', "max:150"],
        ];
    }

    public function getValidationMessage()
    {
        return [
            "foo.required" => "You must provide Foo to continue",
        ];
    }
}