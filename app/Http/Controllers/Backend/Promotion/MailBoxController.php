<?php
/**
 * Project      : phpExam
 * File Name    : MailBox.php
 * User         : Abu Bakar Siddique
 * Email        : absiddique.live@gmail.com
 * Date[Y/M/D]  : 2019/05/24 11:45 AM
 */

namespace App\Http\Controllers\Backend\Promotion;

use App\Http\Controllers\Backend\BackendController;
use Faker\Generator as Faker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class MailBoxController extends BackendController
{
    protected $emails;

    public function __construct(Faker $faker)
    {
        parent::__construct();

        $number = 50;

        while ($number > 0) {
            $this->emails[] = $faker->email;
            $number--;
        }
    }

    public function index()
    {
        return view("backend.promotion", [
            "emails" => $this->emails,
        ]);
    }

    public function sendMail(Request $request)
    {
        $emails = $request->input('emails');

        if (is_array($emails)) {
            $emails = array_chunk($emails, 10);
        } else {
            $emails = [];
        }

        Cache::put('emails', $emails, now()->addDay());

        return redirect()->back()->with([
            "status" => "Emails pushed in Cache successfully",
        ]);
    }
}