<?php
/**
 * Project      : phpExam
 * File Name    : Dashboard.php
 * User         : Abu Bakar Siddique
 * Email        : absiddique.live@gmail.com
 * Date[Y/M/D]  : 2019/05/24 10:37 AM
 */

namespace App\Http\Controllers\Backend;

class DashboardController extends BackendController
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('backend.home');
    }
}