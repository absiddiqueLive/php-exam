<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class CheckFoo
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = Route::currentRouteName();
        $foo   = Session::get('foo');

        if (!$foo && $route != "setFoo") {
            return redirect()->route("setFoo")->withErrors([
                "warning" => "You must set Foo to continue",
            ]);
        } elseif ($foo && $route == "setFoo") {
            return redirect()->route("dashboard");
        }

        return $next($request);
    }
}