<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route("login");
});

Auth::routes();

Route::middleware(['auth', 'hasFoo'])->group(function () {
    Route::get('/dashboard', 'Backend\DashboardController@index')->name('dashboard');

    //Route for Foo
    Route::get('/setfoo', 'Backend\SetFooController@setFoo')->name('setFoo');
    Route::post('/setfoo', 'Backend\SetFooController@storeSetFoo')->name('setFoo');
    Route::any('/getting', 'Backend\GettingController@index')->name('getting');

    Route::get('/promotion', 'Backend\Promotion\MailBoxController@index')->name('promotion');
    Route::post('/promotion', 'Backend\Promotion\MailBoxController@sendMail')->name('sendMail');
});